﻿
using DG.Tweening.Core.Easing;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    private objectPooler _objectPooler;

    public static int nbBullet = 2;
    public static int nbBulletAllow = 2;
    
    private int _nbBullet2;
    public static int nbMissile = 1;
    
    public static bool allowMissile = false;
    public static bool doubleShootEnabled = false;
    public static bool shootLaser = false;
    
    private bool _normalShoot=true;
    
    private void Start()
    {
        _objectPooler=objectPooler.instance;
    }
    
    // Update is called once per frame
    void Update()
    {
        _nbBullet2 = nbBullet;
        //permet de vérifier quand le joueur essaie de tirer si il a encore des munitions.
        if (Input.GetKeyDown(KeyCode.F))
        {
            //chaque if correspond au type de bonus que le joueur a pour voir quelle munition il est autoriser a utiliser.
            if (shootLaser)
            {
                _normalShoot = false;
            }
            else
            {
                _normalShoot = true;
            }
            
            if (_normalShoot && nbBullet <=nbBulletAllow && nbBullet >0)
            {
                Debug.Log(nbBullet);
                nbBullet--;
                _objectPooler.SpawnFromPool("bullet", transform.position, Quaternion.identity);
            }

            if (allowMissile && nbMissile <=1 && nbMissile>0)
            {
                nbMissile--;
                _objectPooler.SpawnFromPool("missile", transform.position, Quaternion.identity);
            }

            if (doubleShootEnabled && _nbBullet2 <=nbBulletAllow && _nbBullet2 >0)
            {
                _objectPooler.SpawnFromPool("bullet2", transform.position, Quaternion.identity);
            }

            if (shootLaser && nbBullet <= nbBulletAllow && nbBullet > 0)
            {            
                nbBullet--;
                _objectPooler.SpawnFromPool("laser", transform.position, Quaternion.identity);
            }
        }
    }
}