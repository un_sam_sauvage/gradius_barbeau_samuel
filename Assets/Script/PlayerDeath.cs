﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDeath : MonoBehaviour
{
    private float _countdownEndDeath = .5f;

    private bool _startDeath=false;

    public string sceneNameToLoad;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && !PlayerShield.shieldIsOn)
        {
            _startDeath = true;
        }

        if (_startDeath)
        {
            gameObject.GetComponent<Animator>().enabled =true;
            _countdownEndDeath -= Time.deltaTime;
            if (_countdownEndDeath <= 0)
            {
                gameObject.transform.DOScale(new Vector3(0,0,0),.5f );
            }

            if (_countdownEndDeath < -2)
            {
                SceneManager.LoadScene(sceneNameToLoad);
            }
        }
    }
}