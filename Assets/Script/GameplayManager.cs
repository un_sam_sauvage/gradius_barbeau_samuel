﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    public Sprite[] selectedBonus;
    public Sprite[] normalBonus;
    private GameObject[] _imageBonus;
    public Sprite neutralBlue;
    public Sprite neutralYellow;

    public static int IndexBonus = -1;
    
    public static GameplayManager instance;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        _imageBonus = GameObject.FindGameObjectsWithTag("ImageBonus");
    }

    // Update is called once per frame
    void Update()
    {
//permet de gérer l'afficage de l'UI en fonction du nombre de bonus que l'on a récolté.
        if (Input.GetKeyDown(KeyCode.A))
        {
            IndexBonus++;
            if (IndexBonus == 6)
            {
                BonusManager.defaultShield = true;
                _imageBonus[IndexBonus-1].GetComponent<Image>().sprite = normalBonus[IndexBonus-1];
                IndexBonus = 0;
            }
            
            
            
            if (IndexBonus > 0 && _imageBonus[IndexBonus-1].GetComponent<Image>().sprite != neutralYellow)
            {
                _imageBonus[IndexBonus-1].GetComponent<Image>().sprite = normalBonus[IndexBonus-1];
            }
            else if (IndexBonus > 0)
            {
                _imageBonus[IndexBonus-1].GetComponent<Image>().sprite = neutralBlue
                    ;

            }
            
            if (_imageBonus[IndexBonus].GetComponent<Image>().sprite != neutralBlue)
            {
                _imageBonus[IndexBonus].GetComponent<Image>().sprite = selectedBonus[IndexBonus];
            }
            else
            {
                _imageBonus[IndexBonus].GetComponent<Image>().sprite = neutralYellow;
            }

        }

        if (IndexBonus < 0)
        {
            for (int i = 0; i < 6; i++)
            {
                if (_imageBonus[i].GetComponent<Image>().sprite != neutralBlue)
                {
                    _imageBonus[i].GetComponent<Image>().sprite = normalBonus[i];
                }
            }
        }
    }

    public void SetNeutral(int index)
    {
        _imageBonus[index].GetComponent<Image>().sprite = neutralBlue;
        IndexBonus = -1;
    }

    public void SetNormal(int index)
    {
        _imageBonus[index].GetComponent<Image>().sprite = normalBonus[index];
    }
}
