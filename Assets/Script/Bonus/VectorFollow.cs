﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorFollow : MonoBehaviour
{
    private Transform _playerTransform;

    private Vector3 _lookDirection;
    public float speed;
    public float distanceMin;

    // Start is called before the first frame update
    void Start()
    {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //permet a l'option de suivre les déplacements du player tout en restant a une distance donée
        _lookDirection = (_playerTransform.position - transform.position).normalized;
        if ((transform.position - _playerTransform.position).magnitude > distanceMin)
        {
            transform.Translate(_lookDirection.x*speed*Time.deltaTime,_lookDirection.y*speed*Time.deltaTime,0);
        }
    }
}
