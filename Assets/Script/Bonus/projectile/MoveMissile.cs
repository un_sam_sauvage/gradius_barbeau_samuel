﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMissile : MonoBehaviour
{
    private Vector2 _screenBounds;

    public float speed;

    // Update is called once per frame
    private void Start()
    {
        _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    void Update()
    {
        transform.Translate((Vector3.right+Vector3.down) * (Time.deltaTime * speed));
        if (transform.position.x > _screenBounds.x)
        {
            PlayerShoot.nbMissile++;
            gameObject.SetActive(false);
        }
    }
}
