﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusManager : MonoBehaviour
{
    private int _nbBonus;

    private bool _test;

    private GameplayManager _gameplayManager;
    private objectPooler _objectPooler;

    private int _nbOption;

    public static bool defaultShield;
    private void Start()
    {
        _gameplayManager = GameplayManager.instance;
        _objectPooler = objectPooler.instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (defaultShield)
        {
            _objectPooler.SpawnFromPool("Shield",
                GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position +
                new Vector3(1, 0, 0), Quaternion.identity);
            _gameplayManager.SetNeutral(5);
            defaultShield = false;
        }
        _nbBonus = GameplayManager.IndexBonus;
        if (Input.GetKeyDown(KeyCode.D))
        {

            //permet de gérer tout les bonus
            switch (_nbBonus)
            {
                //speed
                case 0:
                    GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMove>().speed += 1;
                    break;
                //missile
                case 1:
                    _gameplayManager.SetNeutral(1);
                    PlayerShoot.allowMissile = true;
                    break;
                //double shoot
                case 2:
                    _gameplayManager.SetNeutral(2);
                    _gameplayManager.SetNormal(3);
                    PlayerShoot.doubleShootEnabled = true;
                    PlayerShoot.shootLaser = false;
                    PlayerShoot.nbBullet = 1;
                    PlayerShoot.nbBulletAllow = 1;
                    OptionShoot._nbBulletOption = 1;
                    OptionShoot._nbBulletAllow = 1;
                    break;
                //laser
                case 3:
                    _gameplayManager.SetNeutral(3);
                    _gameplayManager.SetNormal(2);
                    PlayerShoot.shootLaser = true;
                    PlayerShoot.doubleShootEnabled = false;
                    PlayerShoot.nbBullet = 2;
                    PlayerShoot.nbBulletAllow = 2;
                    OptionShoot._nbBulletOption = 2;
                    OptionShoot._nbBulletAllow = 2;
                    break;
                //option
                case 4:
                    _nbOption++;
                    if (_nbOption == 1)
                    {
                        _objectPooler.SpawnFromPool("Option",
                            GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position,
                            Quaternion.identity);
                        _gameplayManager.SetNeutral(4);
                        _gameplayManager.SetNormal(4);
                    }
                    else if (_nbOption == 2)
                    {
                        _gameplayManager.SetNeutral(4);
                        _objectPooler.SpawnFromPool("Option2",
                            GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position,
                            Quaternion.identity);
                        _gameplayManager.SetNeutral(4);
                    }

                    break;
                //shield
                case 5:
                    _objectPooler.SpawnFromPool("Shield",
                        GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position +
                        new Vector3(1, 0, 0), Quaternion.identity);
                    _gameplayManager.SetNeutral(5);
                    break;
            }
        }
    }
}
