﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShield : MonoBehaviour
{
    private int _damageTook;
    private Animator _animator;
    public static bool shieldIsOn;
    private void Start()
    {
        shieldIsOn = true;
        _animator = gameObject.GetComponent<Animator>();
    }

    private void Update()
    {
        transform.position = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position + new Vector3(.82f,0,0);
        if (Input.GetKeyDown(KeyCode.Q))
        {
            GetHit();
        }
    }

    void GetHit()
    {
        _damageTook++;
        if (_damageTook==2)
        {
            _animator.SetBool("Break",true);
        }

        if (_damageTook >= 4)
        {
            shieldIsOn = false;
            gameObject.SetActive(false);
        }
    }
}
