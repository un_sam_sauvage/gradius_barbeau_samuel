﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionShoot : MonoBehaviour
{
    private objectPooler _objectPooler;

    public static int _nbBulletOption = 2;
    public static int _nbBulletAllow = 2;

    public static int _nbBullet2Option;
    public static int _nbMissile = 1;
    

    private bool _normalShoot = true;

    private void Start()
    {
        _objectPooler = objectPooler.instance;
    }

    // Update is called once per frame
    void Update()
    {
        _nbBullet2Option = _nbBulletOption;
        //c'est la même fonction que pour le player
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (PlayerShoot.shootLaser)
            {
                _normalShoot = false;
            }
            else
            {
                _normalShoot = true;
            }

            if (_normalShoot && _nbBulletOption <= _nbBulletAllow && _nbBulletOption > 0)
            {
                _nbBulletOption--;
                _objectPooler.SpawnFromPool("bulletOption", transform.position, Quaternion.identity);
            } 
            
            if (PlayerShoot.allowMissile && _nbMissile <= 1 && _nbMissile > 0)
            {
                _objectPooler.SpawnFromPool("MissileOption", transform.position, Quaternion.identity);
                _nbMissile--;
            }

            if (PlayerShoot.doubleShootEnabled && _nbBullet2Option <= _nbBulletAllow && _nbBullet2Option > 0)
            {
                _objectPooler.SpawnFromPool("bullet2Option", transform.position, Quaternion.identity);
            }

            if (PlayerShoot.shootLaser && _nbBulletOption <= _nbBulletAllow && _nbBulletOption > 0)
            {
                _nbBulletOption--;
                _objectPooler.SpawnFromPool("laserOption", transform.position, Quaternion.identity);
            }
        }
    }
}
