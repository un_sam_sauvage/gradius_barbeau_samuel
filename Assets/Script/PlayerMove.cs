﻿
using UnityEngine;


public class PlayerMove : MonoBehaviour
{
    
    private float _objectWidth;
    private float _objectHeight;
    
    private Vector3 _newPos ;
    private Vector3 _screenBounds;
    
    public float speed;

    public Sprite[] anim;
    // Start is called before the first frame update
    void Start()
    {
        _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        _objectWidth = transform.GetComponent<SpriteRenderer>().bounds.size.x /2;
        _objectHeight = transform.GetComponent<SpriteRenderer>().bounds.size.y /2;
    }

    // Update is called once per frame
    void Update()
    {
        //permet au vaisseau de se déplacer sans sortir de l'écran 
        _newPos = (new Vector3(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"),0) * (Time.deltaTime * speed));
        transform.Translate(_newPos);
        transform.position=new Vector3(Mathf.Clamp(transform.position.x,_screenBounds.x*-1 + _objectWidth,_screenBounds.x - _objectWidth),Mathf.Clamp(transform.position.y,-_screenBounds.y+_objectHeight + 1.6f,_screenBounds.y - _objectHeight),0);
        
        //permet de faire l'animation du vaisseau en fonction de la direction
        if (Input.GetAxis("Vertical") > 0)
        {
            gameObject.transform.GetComponent<SpriteRenderer>().sprite = anim[1];
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            gameObject.transform.GetComponent<SpriteRenderer>().sprite = anim[2];
        }
        else
        {
            gameObject.transform.GetComponent<SpriteRenderer>().sprite = anim[0];
        }
    }
}